import React, { Component } from 'react';
import {Treemap, LineChart, Pie} from 'd3-react-charts';
import './App.css';


function randomize(data) {
  return data.map(d => (
    {name: d.name, value: Math.round(d.value* Math.random()), category: d.category}
  ))
}

function randomLinedata() {
  const arr = [];
  for(let i =0;i< 100; i+=1){
    arr.push(i);
  }
  const date = new Date();
  return arr.map(i=>{
    const d = new Date();
    d.setTime(date.getTime() + i*86400000);
    return  ({
      date: d,
      Sinus: i*13.0 + Math.sin(i/4)*50-500,
      Randomized: i*Math.sqrt(i*2)+ Math.random()*40-500,
    });
  });
}


const testPieData = [
  {name: 'Windows', value: 1000, category: 'PC'},
  {name: 'OSX', value: 1000, category: 'PC'},
  {name: 'Linux', value: 1000, category: 'PC'},
  {name: 'Android', value: 1000, category: 'Mobile'},
  {name: 'iOS', value: 1000, category: 'Mobile'},
  {name: 'WinON', value: 1000, category: 'Mobile'},
  {name: 'AndroidX', value: 1000, category: 'Tablet'},
  {name: 'iOSX', value: 1000, category: 'Tablet'},
];

class App extends Component {

  constructor(){
    super();
    this.state = {
      lineChartData: [],
      lineChartData2: [],
    }

    setTimeout(()=>{
      const lineChartData = randomLinedata();
      const lineChartData2 = lineChartData.map((d,i)=> ({Sinus:d.Sinus,Random: d.Randomized,day: i}));

      this.setState({lineChartData,lineChartData2})
    },500)
  }

  render() {
    return (
      <div className="App">
        <div className="Row">

          <div className="Col">
            <h2>Treemap, default</h2>
            <Treemap
              id="Tree1"
              data={randomize(testPieData)}
              width={600}
              height={300}
            />
          </div>
          <div className="Col">
            <h2>Treemap</h2>
            <Treemap
              id="Tree1"
              data={randomize(testPieData)}
              width={600}
              height={300}
              colorRange={{
                PC: '#94d920',
                Mobile: '#5923f4',
                Tablet: '#a54000'}}
            />
            <p>{`colorRange={{
                PC: '#94d920',
                Mobile: '#5923f4',
                Tablet: '#a54000'}}`}</p>
          </div>
        </div>
        <div className="Row">

          <div className="Col">
            <h2>Pie chart, default</h2>
            <Pie
              id="Pie1"
              data={randomize(testPieData)}
              width={350}
              height={200}
            />
          </div>

          <div className="Col">
            <h2>Pie chart, no labels, colorRange</h2>
            <Pie
              id="Pie2"
              data={randomize(testPieData)}
              width={300}
              height={300}
              colorRange={{'Windows': 'blue', 'OSX': 'red'}}
              labels={false}
            />
            <p>{`colorRange={{'Windows': 'blue', 'OSX': 'red'}}`}</p>
            <p>{`labels={false}`}</p>

          </div>

          <div className="Col">
            <h2>Pie chart, inner radius</h2>
            <Pie
              id="Pie3"
              data={randomize(testPieData)}
              width={350}
              height={200}
              innerRadius={0.9}
              totalColor="#000"
              fontSize={15}
              expandHighlightRadius={1.05}
            />
            <p>{`innerRadius={0.9}`}</p>
            <p>{`totalColor="#000"`}</p>
            <p>{`fontSize={15}`}</p>
            <p>{`expandHighlightRadius={1.05}`}</p>
          </div>

        </div>
        <br/>
        <div className="Row">
          <div className="Col">
            <h2>Line chart default</h2>
            <LineChart
              id="Line1"
              data={this.state.lineChartData}
              width={800}
              height={400}
            />
          </div>
          <div className="Col">
            <h2>Line chart days</h2>
            <LineChart
              id="Line2"
              data={this.state.lineChartData2}
              yName="day"
              yType="number"
              colorRange={{Sinus: 'blue', Random: 'orange'}}
              width={400}
              height={400}
            />
            <p>{`colorRange={{Sinus: 'blue', Random: 'orange'}}`}</p>
            <p>{`yName="day"`}</p>
            <p>{`yType="number"`}</p>
          </div>
        </div>
      <br/>

      </div>
    );
  }
}

export default App;
